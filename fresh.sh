#!/bin/bash

./yii migrate/fresh --interactive=0
./yii migrate --migrationPath=@yii/rbac/migrations --interactive=0
./yii auth/init
./yii user/create admin
./yii config
