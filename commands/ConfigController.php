<?php

namespace app\commands;


use app\models\AllowField;
use app\models\ChannelAccessLevel;
use app\models\ChannelType;
use yii\console\Controller;

class ConfigController extends Controller {
    public function actionIndex() {
        $ct = new ChannelType();
        $ct->name = 'vk';
        $ct->title = 'Вконтакте';
        $ct->save();

        $cal = new ChannelAccessLevel();
        $cal->user_id = 1;
        $cal->channel_type_id = $ct->id;
        $cal->save();

        $af = new AllowField();
        $af->channel_types_id = $ct->id;
        $af->name = 'link';
        $af->type = 'string';
        $af->title = 'Ссылка';
        $af->save();

        $af = new AllowField();
        $af->channel_types_id = $ct->id;
        $af->name = 'price';
        $af->type = 'integer';
        $af->title = 'Ценв';
        $af->save();

        $af = new AllowField();
        $af->channel_types_id = $ct->id;
        $af->name = 'date';
        $af->type = 'datetime';
        $af->title = 'Дата';
        $af->save();
    }
}