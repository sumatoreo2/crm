<?php

namespace app\commands;

use app\models\User;
use yii\console\Controller;

/**
 * This command contains action for work with users
 */
class UserController extends Controller {
    /**
     * This command echoes user list
     * @return int Exit code
     */
    public function actionIndex() {
        $users = User::find()->select(['username', 'email'])->all();
    }

    /**
     * Create default admin user
     * @throws \yii\base\Exception
     */
    public function actionCreate($role = 'admin') {
        $user = new User();

        if ($role === 'admin') {
            $user->username = 'Admin';
            $user->email = 'user@default.com';
            $user->password = \Yii::$app->getSecurity()->generatePasswordHash('123');
            $user->save(false);

            $auth = \Yii::$app->authManager;
            $authRole = $auth->getRole('admin');
            $auth->assign($authRole, $user->getId());
        } else if ($role === 'manager') {
            $user->username = 'Manager';
            $user->email = 'manager@default.com';
            $user->password = \Yii::$app->getSecurity()->generatePasswordHash('123');
            $user->save(false);

            $auth = \Yii::$app->authManager;
            $authRole = $auth->getRole('manager');
            $auth->assign($authRole, $user->getId());
        }
    }
}