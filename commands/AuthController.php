<?php

namespace app\commands;

use app\rbac\ChannelAuthor;
use app\rbac\ChannelAccess;
use yii\console\Controller;
use Yii;

class AuthController extends Controller {
    public function actionInit() {
        $auth = Yii::$app->authManager;

        /**
         * Permissions
         */

        $create = $auth->createPermission('create');
        $create->description = 'Создание';
        $auth->add($create);

        $update = $auth->createPermission('update');
        $update->description = 'Обновление';
        $auth->add($update);

        $delete = $auth->createPermission('delete');
        $delete->description = 'Удаление';
        $auth->add($delete);

        /**
         * Roles
         */

        $manager = $auth->createRole('manager');
        $manager->description = 'Менеджер';
        $auth->add($manager);

        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';
        $auth->add($admin);
        $auth->addChild($admin, $create);
        $auth->addChild($admin, $update);
        $auth->addChild($admin, $delete);

        /**
         * Extension rules
         */

        $rule = new ChannelAccess();
        $auth->add($rule);

        $canAccess = $auth->createPermission('canAccess');
        $canAccess->ruleName = $rule->name;
        $auth->add($canAccess);

        $auth->addChild($canAccess, $create);
        $auth->addChild($manager, $canAccess);

        $rule = new ChannelAuthor();
        $auth->add($rule);

        $updateOwnChannel = $auth->createPermission('updateOwnChannel');
        $updateOwnChannel->ruleName = $rule->name;
        $auth->add($updateOwnChannel);

        $auth->addChild($updateOwnChannel, $update);
        $auth->addChild($manager, $updateOwnChannel);

        $deleteOwnChannel = $auth->createPermission('deleteOwnChannel');
        $deleteOwnChannel->ruleName = $rule->name;
        $auth->add($deleteOwnChannel);

        $auth->addChild($deleteOwnChannel, $delete);
        $auth->addChild($manager, $deleteOwnChannel);
    }
}