<?php

use yii\db\Migration;

/**
 * Class m180725_180059_channel_types
 */
class m180725_180059_channel_types extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('channel_types', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull()->unique(),
            'title' => $this->string()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180725_180059_channel_types cannot be reverted.\n";

        $this->dropTable('channel_types');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180725_180059_channel_types cannot be reverted.\n";

        return false;
    }
    */
}
