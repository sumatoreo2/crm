<?php

use yii\db\Migration;

/**
 * Class m180725_180133_channel_attributes
 */
class m180725_180133_channel_attributes extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('channel_attributes', [
            'id' => $this->primaryKey(),
            'channel_id' => $this->integer()->notNull(),
            'allow_fields_id' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()
        ]);

        $this->createIndex(
            'idx_unique_channel_attributes_channel_id',
            'channel_attributes',
            ['channel_id', 'allow_fields_id'],
            true
        );

        $this->addForeignKey(
            'fk_channel_attributes_channel_id',
            'channel_attributes',
            'channel_id',
            'channels',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180725_180133_channel_attributes cannot be reverted.\n";

        $this->dropForeignKey('fk_channel_attributes_channel_id', 'channel_attributes');
        $this->dropIndex('idx_unique_channel_attributes_channel_id', 'channel_attributes');


        $this->dropTable('channel_attributes');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180725_180133_channel_attributes cannot be reverted.\n";

        return false;
    }
    */
}
