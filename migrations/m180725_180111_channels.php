<?php

use yii\db\Migration;

/**
 * Class m180725_180111_channels
 */
class m180725_180111_channels extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('channels', [
            'id' => $this->primaryKey(),
            'channel_type_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()
        ]);

        $this->createIndex(
            'idx_channels_channel_type_id',
            'channels',
            'channel_type_id'
        );

        $this->addForeignKey(
            'fk_channels_channel_type_id',
            'channels',
            'channel_type_id',
            'channel_types',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx_channels_user_id',
            'channels',
            'user_id'
        );

        $this->addForeignKey(
            'fk_channels_user_id',
            'channels',
            'user_id',
            'users',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180725_180111_channels cannot be reverted.\n";

        $this->dropForeignKey('fk_channels_user_id', 'channels');
        $this->dropForeignKey('fk_channels_channel_type_id', 'channels');

        $this->dropIndex('idx_channels_user_id', 'channels');
        $this->dropIndex('idx_channels_channel_type_id', 'channels');

        $this->dropTable('channels');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180725_180111_channels cannot be reverted.\n";

        return false;
    }
    */
}
