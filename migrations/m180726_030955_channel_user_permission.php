<?php

use yii\db\Migration;

/**
 * Class m180726_030955_channel_user_permission
 */
class m180726_030955_channel_user_permission extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('channel_access_level', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'channel_type_id' => $this->integer()->notNull()
        ]);

        $this->createIndex(
            'idx_channel_access_level_user_id',
            'channel_access_level',
            ['user_id', 'channel_type_id'],
            true
        );

        $this->addForeignKey(
            "fk_channel_access_level_user_id",
            "channel_access_level",
            "user_id",
            "users",
            "id"
        );

        $this->addForeignKey(
            "fk_channel_access_level_channel_type_id",
            "channel_access_level",
            "channel_type_id",
            "channel_types",
            "id",
            "CASCADE",
            "CASCADE"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180726_030955_channel_user_permission cannot be reverted.\n";

        $this->dropForeignKey('fk_channel_access_level_channel_type_id', 'channel_access_level');
        $this->dropForeignKey('fk_channel_access_level_user_id', 'channel_access_level');
        $this->dropIndex('idx_channel_access_level_user_id', 'channel_access_level');

        $this->dropTable('channel_access_level');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180726_030955_channel_user_permission cannot be reverted.\n";

        return false;
    }
    */
}
