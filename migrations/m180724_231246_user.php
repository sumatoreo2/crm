<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180724_231246_user
 */
class m180724_231246_user extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'password' => $this->string()->notNull(),
            'auth_key' => $this->string(),
            'access_token' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180724_231246_user cannot be reverted.\n";

        $this->dropTable('users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180724_231246_user cannot be reverted.\n";

        return false;
    }
    */
}
