<?php

use yii\db\Migration;

/**
 * Class m180726_072120_allow_fields
 */
class m180726_072120_allow_fields extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('allow_fields', [
            'id' => $this->primaryKey(),
            'channel_types_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'title' => $this->string()->notNull()
        ]);

        $this->createIndex(
            'idx_allow_fields_channel_types_id_name',
            'allow_fields',
            ['channel_types_id', 'name'],
            true
        );

        $this->addForeignKey(
            'fk_allow_fields_channel_types_id',
            'allow_fields',
            'channel_types_id',
            'channel_types',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180726_072120_allow_fields cannot be reverted.\n";

        $this->dropForeignKey('fk_allow_fields_channel_types_id', 'allow_fields');
        $this->dropIndex('fk_allow_fields_channel_types_id', 'allow_fields');
        $this->dropTable('allow_fields');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180726_072120_allow_fields cannot be reverted.\n";

        return false;
    }
    */
}
