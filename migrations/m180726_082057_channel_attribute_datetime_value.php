<?php

use yii\db\Migration;

/**
 * Class m180726_082057_channel_attribute_datetime_value
 */
class m180726_082057_channel_attribute_datetime_value extends Migration {
    private $type = "datetime";
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable("channel_attribute_{$this->type}_value", [
            "id" => $this->primaryKey(),
            "channel_attribute_id" => $this->integer(),
            "value" => $this->dateTime()
        ]);

        $this->createIndex(
            "idx_attribute_{$this->type}_channel_attribute_id",
            "channel_attribute_{$this->type}_value",
            "channel_attribute_id"
        );

        $this->addForeignKey(
            "fk_attribute_{$this->type}_channel_attribute_id",
            "channel_attribute_{$this->type}_value",
            "channel_attribute_id",
            "channel_attributes",
            "id",
            "CASCADE",
            "CASCADE"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180726_082057_channel_attribute_datetime_value cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180726_082057_channel_attribute_datetime_value cannot be reverted.\n";

        return false;
    }
    */
}
