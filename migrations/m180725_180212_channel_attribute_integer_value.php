<?php

use yii\db\Migration;

/**
 * Class m180725_180212_channel_attribute_integer_value
 */
class m180725_180212_channel_attribute_integer_value extends Migration {
    private $type = "integer";
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable("channel_attribute_{$this->type}_value", [
            "id" => $this->primaryKey(),
            "channel_attribute_id" => $this->integer(),
            "value" => $this->integer()
        ]);

        $this->createIndex(
            "idx_attribute_{$this->type}_channel_attribute_id",
            "channel_attribute_{$this->type}_value",
            "channel_attribute_id"
        );

        $this->addForeignKey(
            "fk_attribute_{$this->type}_channel_attribute_id",
            "channel_attribute_{$this->type}_value",
            "channel_attribute_id",
            "channel_attributes",
            "id",
            "CASCADE",
            "CASCADE"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180725_180205_channel_attribute_{$this->type}_value cannot be reverted.\n";

        $this->dropForeignKey("fk_attribute_{$this->type}_channel_attribute_id", "channel_attribute_{$this->type}_value");
        $this->dropIndex("idx_attribute_{$this->type}_channel_attribute_id", "channel_attribute_{$this->type}_value");

        $this->dropTable("channel_attribute_{$this->type}_value");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180725_180212_channel_attribute_integer_value cannot be reverted.\n";

        return false;
    }
    */
}
