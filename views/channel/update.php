<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

$this->title = 'Редактировани записи';
$this->params['breadcrumbs'][] = ['label' => 'Каналы', 'url' => Url::to(['channel/index'])];
$this->params['breadcrumbs'][] = ['label' => $channelType->title, 'url' => Url::to(['channel/view', 'id' => $channelType->id])];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <?php
            $form = ActiveForm::begin([
                'id' => 'form-create',
                'enableClientValidation' => false,
                'options' => []
            ]);
            ?>
            <div class="col-xs-6">
                <p class="h3">Основная информация</p><br/>
                <?php
                /**
                 * @var \app\models\AllowField $allowField
                 */
                foreach ($allowFields as $allowField): ?>
                    <?php if ($allowField->type === \app\models\AllowField::TYPE_DATETIME): ?>
                        <div class="form-group field-dynamicmodel-<?= $allowField->name ?> <?= !$model->hasErrors($allowField->name) ?: 'has-error' ?>">
                            <label class="control-label" for="dynamicmodel-<?= $allowField->name ?>"><?= $allowField->title ?></label>
                            <?= DateTimePicker::widget([
                                'model' => $model,
                                'attribute' => $allowField->name,
                                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd hh:ii:ss',
                                    'autoclose' => true,
                                    'todayHighlight' => true
                                ]
                            ]); ?>
                            <div class="help-block">
                                <?= $model->hasErrors($allowField->name) ? $model->getErrors($allowField->name)[0] : '' ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <?= $form->field($model, $allowField->name) ?>
                    <?php endif; ?>
                <?php endforeach; ?>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
