<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Типы каналов';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-default">
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                'name',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'template' => '{create}{view}',
                    'buttons' => [
                        'create' => function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-plus"></span> Добавить', $url, [
                                'title' => Yii::t('yii', 'Добавить'),
                                'class' => 'btn'
                            ]);
                        }
                    ]
                ]
            ]
        ]) ?>
    </div>
</div>