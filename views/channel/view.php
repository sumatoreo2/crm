<?php

use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = $channelType->title;
$this->params['breadcrumbs'][] = ['label' => 'Каналы', 'url' => Url::to(['channel/index'])];;
$this->params['breadcrumbs'][] = $channelType->title;
?>

<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <div class="btn-group pull-right">
            <a href="<?= Url::to(['channel/create', 'id' => $id]); ?>" type="button" class="btn btn-default">Добавить запись</a>
        </div>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns
        ]) ?>
    </div>
</div>