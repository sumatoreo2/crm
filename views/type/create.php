<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Создание типа канала';
$this->params['breadcrumbs'][] = ['label' => 'Типы каналов', 'url' => Url::to(['type/index'])];
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin([
    'id' => 'channel-create',
    'enableClientValidation' => false,
    'options' => []
]);

?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6">
                <p class="h3">Основная информация</p><br/>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'title') ?>

                <div class="form-group">
                    <?= Html::submitButton('Создать', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>
