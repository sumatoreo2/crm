<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Редактирование ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Типы каналов', 'url' => Url::to(['type/index'])];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <div class="btn-group pull-right">
            <a href="<?= Url::to(['fields/index', 'channel' => $id]); ?>" type="button" class="btn btn-default">Список полей</a>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'channel-update',
                    'enableClientValidation' => false,
                    'options' => []
                ]);
                ?>
                <p class="h3">Основная информация</p><br/>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'title') ?>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>