<?php

use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Типы каналов';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <div class="btn-group pull-right">
            <a href="<?= Url::to(['type/create']); ?>" type="button" class="btn btn-default">Новый канал</a>
        </div>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'title',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'template' => '{update}{delete}',
                ]
            ]
        ]) ?>
    </div>
</div>