<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Редактиирование пользователя ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => Url::to(['user/index'])];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <?php
                $form = ActiveForm::begin([
                    'id' => 'user-update',
                    'enableClientValidation' => false,
                    'options' => []
                ]);
            ?>
            <div class="col-xs-6">
                <p class="h3">Основная информация</p><br/>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'email') ?>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <div class="col-xs-6">
                <p class="h3">Разрешения</p><br/>
                <?= $form->field($model, 'role')
                    ->radioList(['admin' => 'Администратор', 'manager' => 'Менеджер']) ?>
                <?= $form->field($model, 'channels')
                    ->checkboxList(\yii\helpers\ArrayHelper::map($types, 'id', 'title')) ?>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <?php
            $form = ActiveForm::begin([
                'id' => 'user-password-change',
                'enableClientValidation' => false,
            ]);
            ?>
            <div class="col-xs-6">
                <p class="h3">Смена пароля</p><br />
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'password_confirm')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Сменить', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
