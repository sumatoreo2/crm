<?php

use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <div class="btn-group pull-right">
            <a href="<?= Url::to(['user/create']); ?>" type="button" class="btn btn-default">Новый пользователь</a>
        </div>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'username',
                'email',
                [
                    'header' => 'Роль',
                    'value' => function ($data) {
                        $roles = \Yii::$app->authManager->getRolesByUser($data->id);
                        $descriptions = array_column($roles, 'description');
                        $descriptionsJoin = join(',', $descriptions);

                        return $descriptionsJoin;
                    },
                ],
                [
                    'header' => 'Каналы',
                    'value' => function ($data) {
                        $channels = \app\models\User::getAvailableChannels($data->id);
                        $title = [];

                        foreach ($channels as $channel) {
                            $title[] = $channel->title;
                        }

                        $titleJoin = join(',', $title);

                        return $titleJoin ? $titleJoin : 'Каналы отсутствуют';
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'template' => '{update}{delete}',
                ]
            ]
        ]) ?>
    </div>
</div>