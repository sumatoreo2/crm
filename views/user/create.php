<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Создание пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => Url::to(['user/index'])];
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin([
    'id' => 'user-create',
    'enableClientValidation' => false,
    'options' => []
]);

?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6">
                <p class="h3">Основная информация</p><br/>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'password_confirm')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Создать', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <div class="col-xs-6">
                <p class="h3">Разрешения</p><br/>
                <?= $form->field($model, 'role')
                    ->radioList(['admin' => 'Администратор', 'manager' => 'Менеджер']) ?>
                <?= $form->field($model, 'channels')
                    ->checkboxList(\yii\helpers\ArrayHelper::map($types, 'id', 'title')) ?>
            </div>
        </div>
    </div>
</div>
