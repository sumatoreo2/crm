<?php

use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Список полей ' . $channelName;
$this->params['breadcrumbs'][] = ['label' => 'Типы каналов', 'url' => Url::to(['type/index'])];
$this->params['breadcrumbs'][] = ['label' => 'Редактирование ' . $channelName, 'url' => Url::to(['type/update', 'id' => $channel])];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <div class="btn-group pull-right">
            <a href="<?= Url::to(['fields/create', 'channel' => $channel]); ?>" type="button" class="btn btn-default">Добавить поле</a>
        </div>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                'name',
                'type',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span> Удалить',
                                Url::to(['fields/delete', 'channel' => $model->channel_types_id, 'id' => $model->id]), [
                                'title' => Yii::t('yii', 'Удалить'),
                                'class' => 'btn'
                            ]);
                        }
                    ]
                ]
            ]
        ]) ?>
    </div>
</div>