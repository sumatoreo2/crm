<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Новое поле ';
$this->params['breadcrumbs'][] = ['label' => 'Типы каналов', 'url' => Url::to(['type/index'])];
$this->params['breadcrumbs'][] = ['label' => 'Редактирование ' . $channelName, 'url' => Url::to(['type/update', 'id' => $channel])];
$this->params['breadcrumbs'][] = ['label' => 'Список полей ' . $channelName, 'url' => Url::to(['fields/index', 'channel' => $channel])];
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin([
    'id' => 'form-create',
    'enableClientValidation' => false,
    'options' => []
]);

?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6">
                <p class="h3">Основная информация</p><br/>
                <?= $form->field($model, 'title') ?>
                <?= $form->field($model, 'name')->input('text') ?>
                <?= $form->field($model,'type')->radioList($model->getTypes()) ?>

                <div class="form-group">
                    <?= Html::submitButton('Создать', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>
