<?php

namespace app\models;


use yii\db\ActiveRecord;

class ChannelAtribute extends ActiveRecord {
    public static function tableName() {
        return 'channel_attributes';
    }
}