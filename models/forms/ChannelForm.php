<?php


namespace app\models\forms;

use yii\base\Model;

class ChannelForm extends Model {
    public $channel_type_id;
    public $user_id;
    public $date;
    public $string;
    public $integer;

    public function rules() {
        return [
            ['date', 'date'],
            ['string', 'string', 'max' => 255],
            ['integer', 'integer']
        ];
    }
}