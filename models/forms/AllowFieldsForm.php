<?php

namespace app\models\forms;


use yii\base\Model;

class AllowFieldsForm extends Model {
    public $channel_types_id;
    public $name;
    public $type;
    public $title;

    public function getTypes() {
        return [
            'integer' => 'Число',
            'string' => 'Сторка',
            'datetime' => 'Дата'
        ];
    }

    public function rules() {
        return [
            ['channel_types_id', 'integer'],
            [['name', 'type', 'title'], 'required'],
            ['name', 'match', 'not' => true, 'pattern' => '/[^a-z]/'],
            ['type', 'in', 'range' => array_keys($this->getTypes())],
            ['title', 'string', 'max' => 255]
        ];
    }

    public function attributeLabels() {
        return [
            'channel_types_id' => 'Тип канала',
            'name' => 'Имя поля (для бд)',
            'type' => 'Тип поля',
            'title' => 'Заголовок поля (для пользователя)'
        ];
    }
}