<?php

namespace app\models\forms;


use app\models\AllowField;
use yii\base\Model;

class DynamicModel extends Model {
    private $fields = [];
    private $rules = [];
    private $labels = [];

    public function setFileds(array $fields) {
        $this->fields = $fields;
    }

    public function setRules(array $rules) {
        $this->rules = $rules;
    }

    public function generate($allowFields) {
        /**
         * @var AllowField $allowField
         */
        foreach ($allowFields as $allowField) {
            $type = $allowField->type;
            $name = $allowField->name;
            $title = $allowField->title;

            $this->labels[$name] = $title;

            $this->fields[$name] = '';

            switch ($type) {
                case 'string': {
                    $this->rules[] = [
                        $name, 'string', 'max' => 255
                    ];

                    break;
                }
                case 'integer': {
                    $this->rules[] = [
                        $name, 'integer'
                    ];

                    break;
                }
                case 'datetime': {
                    $this->rules[] = [
                        $name, 'datetime', 'format' => 'php:Y-m-d H:i:s'
                    ];

                    break;
                }
            }
        }
    }

    public function __get($name) {
        if (isset($this->fields[$name])) {
            return $this->fields[$name];
        }

        return parent::__get($name);
    }

    public function __set($name, $value) {
        if (isset($this->fields[$name])) {
            return $this->fields[$name] = $value;
        }

        return parent::__set($name, $value);
    }

    public function rules() {
        return array_merge(parent::rules(), $this->rules);
    }

    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), $this->labels);
    }
}