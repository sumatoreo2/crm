<?php

namespace app\models\forms;

use yii\base\Model;

class UserForm extends Model {
    CONST SCENARIO_CREATE = 'create';
    CONST SCENARIO_UPDATE = 'update';
    CONST SCENARIO_CHANGE_PASSWORD = 'change_password';

    public $username;
    public $email;
    public $password;
    public $password_confirm;
    public $role;
    public $channels;

    public function scenarios() {
        return [
            self::SCENARIO_CREATE => ['username', 'email', 'role', 'channels', 'password', 'password_confirm'],
            self::SCENARIO_UPDATE => ['username', 'email', 'role', 'channels'],
            self::SCENARIO_CHANGE_PASSWORD => ['password', 'password_confirm']
        ];
    }

    public function rules() {
        return [
            [['username', 'email', 'password', 'password_confirm', 'role', 'channels'], 'required'],
            ['email', 'email'],
            ['role', 'in', 'range' => ['admin', 'manager']],
            ['channels', 'each', 'rule' => [
                    'match', 'not' => true, 'pattern' => '/[^0-9]/'
                ]
            ],
            ['password_confirm', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают']
        ];
    }

    public function attributeLabels() {
        return [
            'username' => 'Имя пользователя',
            'email' => 'Email',
            'password' => 'Пароль',
            'password_confirm' => 'Повторить пароль',
            'role' => 'Роли',
            'channels' => 'Каналы'
        ];
    }
}