<?php

namespace app\models\forms;

use yii\base\Model;

class ChannelTypeForm extends Model {
    public $name;
    public $title;

    public function rules() {
        return [
            [['name', 'title'], 'required'],
            ['name', 'unique', 'targetClass' => 'app\models\ChannelType'],
            ['name', 'string', 'max' => 64],
            ['title', 'string', 'max' => 255],
            ['name', 'match', 'not' => true, 'pattern' => '/[^a-z]/'],
            ['name', 'in', 'not' => true, 'range' => ['id', 'type', 'channel', 'user_id', 'user_name', 'user_email']]
        ];
    }

    public function attributeLabels() {
        return [
            'name' => 'Имя (только a-z)',
            'title' => 'Описание'
        ];
    }
}