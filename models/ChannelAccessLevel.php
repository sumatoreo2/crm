<?php

namespace app\models;

use yii\db\ActiveRecord;

class ChannelAccessLevel extends ActiveRecord {
    public static function tableName() {
        return 'channel_access_level';
    }
}