<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Query;

class Channel extends ActiveRecord {
    public static function tableName() {
        return 'channels';
    }

    public static function getRowByChannel($id) {
        $channel = Channel::findOne(['id' => $id]);
        $allowFields = AllowField::findAll(['channel_types_id' => $channel->channel_type_id]);

        $select =
            "SELECT
                ca.channel_id AS id";

        $from =
            "\nFROM allow_fields af
                INNER JOIN channel_attributes ca ON af.id = ca.allow_fields_id";

        $end =
            "\nWHERE ca.channel_id = $id
             GROUP BY ca.channel_id";

        $tbls = [];

        foreach ($allowFields as $key => $allowField) {
            $tbls[$allowField->type] = [
                'alias' => "t_$key",
                'join' => "LEFT JOIN channel_attribute_{$allowField->type}_value t_$key on ca.id = t_$key.channel_attribute_id"
            ];
        }

        foreach ($allowFields as $allowField) {
            $select .=
                "\n, GROUP_CONCAT(CASE WHEN af.name = '{$allowField->name}' THEN {$tbls[$allowField->type]['alias']}.value ELSE NULL END) AS {$allowField->name}";
        }

        foreach ($tbls as $tbl) {
            $from .=
                "\n {$tbl['join']}";
        }

        return (new Query())
            ->createCommand()
            ->setRawSql($select.$from.$end)
            ->queryAll();
    }

    public static function getRowsByChannelType($id, &$columns = [], $userId) {
        $allowFields = AllowField::findAll(['channel_types_id' => $id]);

        $select =
            "SELECT
                ca.channel_id AS id";

        $from =
            "\nFROM allow_fields af
                INNER JOIN channel_attributes ca ON af.id = ca.allow_fields_id
                INNER JOIN channels c on ca.channel_id = c.id";

        if (\Yii::$app->user->can('admin')) {
            $from .=
                "\nINNER JOIN users u ON c.user_id = u.id";

            $select .=
                "\n,u.username AS user_name,
                 u.email AS user_email";

            $columns[] = [
                'label' => 'Пользователь',
                'attribute' => 'user_name'
            ];

            $columns[] = [
                'label' => 'Почта',
                'attribute' => 'user_email'
            ];

            $end =
                "\nWHERE c.channel_type_id = $id
                   GROUP BY ca.channel_id";
        } else {
            $end =
                "\nWHERE c.channel_type_id = $id AND c.user_id = $userId
                   GROUP BY ca.channel_id";
        }

        $tbls = [];

        foreach ($allowFields as $key => $allowField) {
            $tbls[$allowField->type] = [
                'alias' => "t_$key",
                'join' => "LEFT JOIN channel_attribute_{$allowField->type}_value t_$key on ca.id = t_$key.channel_attribute_id"
            ];
        }

        foreach ($allowFields as $allowField) {
            $select .=
                "\n, GROUP_CONCAT(CASE WHEN af.name = '{$allowField->name}' THEN {$tbls[$allowField->type]['alias']}.value ELSE NULL END) AS {$allowField->name}";

            $columns[] = [
                'label' => $allowField->title,
                'attribute' => $allowField->name
            ];
        }

        foreach ($tbls as $tbl) {
            $from .=
                "\n {$tbl['join']}";
        }

        return $select.$from.$end;
    }
}