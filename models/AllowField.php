<?php

namespace app\models;


use yii\db\ActiveRecord;

class AllowField extends ActiveRecord {
    const TYPE_DATETIME = 'datetime';
    const TYPE_STRING = 'string';
    const TYPE_INTEGER = 'integer';

    public static function tableName() {
        return 'allow_fields';
    }
}