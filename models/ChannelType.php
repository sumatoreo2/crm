<?php

namespace app\models;

use yii\db\ActiveRecord;

class ChannelType extends ActiveRecord {
    public static function tableName() {
        return 'channel_types';
    }
}