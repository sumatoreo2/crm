<?php

namespace app\controllers;

use app\models\AllowField;
use app\models\ChannelType;
use app\models\forms\AllowFieldsForm;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Класс для работы с полями
 * Class FieldsController
 * @package app\controllers
 */
class FieldsController extends Controller {
    /**
     * Разрешения
     * @return array
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete'],
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN]
                    ]
                ],
            ]
        ];
    }

    /**
     * Список полей
     * @param $channel
     * @return string
     */
    public function actionIndex($channel) {
        $channelType = ChannelType::findOne(['id' => $channel]);

        $dataProvider = new ActiveDataProvider([
            'query' => AllowField::find()->where(['channel_types_id' => $channel]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'channelName' => $channelType->title,
            'channel' => $channel
        ]);
    }

    /**
     * Создание поля
     * @param $channel
     * @return string
     */
    public function actionCreate($channel) {
        $channelType = ChannelType::findOne(['id' => $channel]);
        $model = new AllowFieldsForm();
        $model->channel_types_id = $channel;

        if (\Yii::$app->request->post()) {
            $model->load(\Yii::$app->request->post());

            if ($model->validate()) {
                try {
                    $allowFields = new AllowField();
                    $allowFields->setAttributes($model->getAttributes(), false);
                    $allowFields->save();
                } catch (\Exception $exception) {
                    \Yii::$app->session->setFlash('warning', 'Ошибка при добавлении поля');
                    return $this->redirect(['fields/index', 'channel' => $channel]);
                }

                \Yii::$app->session->setFlash('success', 'Поле успешно добавлено');
                return $this->redirect(['fields/index', 'channel' => $channel]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'channel' => $channel,
            'channelName' => $channelType->title,
        ]);
    }

    /**
     * Удаление поля
     * @param $channel
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($channel, $id) {
        try {
            AllowField::findOne(['id' => $id])->delete();
        } catch (\Exception $exception) {
            \Yii::$app->session->setFlash('warning', 'Ошибка при удалении поля');
            return $this->redirect(['fields/index', 'channel' => $channel]);
        } catch (\Throwable $e) {
            \Yii::$app->session->setFlash('warning', 'Ошибка при удалении поля');
            return $this->redirect(['fields/index', 'channel' => $channel]);
        }

        \Yii::$app->session->setFlash('success', 'Поле успешно удалено');
        return $this->redirect(['fields/index', 'channel' => $channel]);
    }
}