<?php

namespace app\controllers;


use app\models\ChannelType;
use app\models\forms\ChannelTypeForm;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Контроллер для управление типами каналов
 * Class TypeController
 * @package app\controllers
 */
class TypeController extends Controller {
    /**
     * Разрешения
     * @return array
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete', 'update'],
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN]
                    ]
                ],
            ]
        ];
    }

    /**
     * Список типов
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => ChannelType::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Создаеие типа
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new ChannelTypeForm();

        if (\Yii::$app->request->post()) {
            $model->load(\Yii::$app->request->post());

            if ($model->validate()) {
                try {
                    $type = new ChannelType();
                    $type->name = $model->name;
                    $type->title = $model->title;
                    $type->save();
                } catch (\Exception $exception) {
                    \Yii::$app->session->setFlash('warning', 'Ошибка при создании типа');
                    return $this->redirect(['type/index']);
                }

                \Yii::$app->session->setFlash('success', 'Тип успешно создан');
                return $this->redirect(['type/index']);
            }
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * Удаление типа
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id) {
        try {
            ChannelType::findOne(['id' => $id])->delete();
        } catch (\Exception $exception) {
            \Yii::$app->session->setFlash('warning', 'Ошибка при удалении типа');
            return $this->redirect(['type/index']);
        } catch (\Throwable $e) {
            \Yii::$app->session->setFlash('warning', 'Ошибка при удалении типа');
            return $this->redirect(['type/index']);
        }

        \Yii::$app->session->setFlash('success', 'Тип успешно удален');
        return $this->redirect(['type/index']);
    }

    /**
     * Обновление типа
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id) {
        $model = ChannelType::findOne(['id' => $id]);
        $form = new ChannelTypeForm();

        $form->setAttributes($model->getAttributes());

        if (\Yii::$app->request->post()) {
            $form->load(\Yii::$app->request->post());

            if ($form->validate()) {
                try {
                    $model->name = $form->name;
                    $model->title = $form->title;
                    $model->save();
                } catch (\Exception $exception) {
                    \Yii::$app->session->setFlash('warning', 'Ошибка при обновлении типа');
                    return $this->redirect(['type/index']);
                }

                \Yii::$app->session->setFlash('success', 'Тип успешно обновлен');
                return $this->redirect(['type/index']);
            }
        }

        return $this->render('update', [
            'model' => $form,
            'id' => $id
        ]);
    }
}