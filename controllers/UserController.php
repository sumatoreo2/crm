<?php

namespace app\controllers;

use app\models\ChannelAccessLevel;
use app\models\ChannelType;
use app\models\forms\UserForm;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Класс для работы со списком пользователей
 * Class UserController
 * @package app\controllers
 */
class UserController extends Controller {
    /**
     * Разрешения
     * @return array
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete', 'update'],
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN]
                    ]
                ],
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Создание пользователя
     * @return string
     */
    public function actionCreate() {
        $model = new UserForm();
        $model->setScenario(UserForm::SCENARIO_CREATE);
        $types = ChannelType::find()->all();

        if (\Yii::$app->request->post()) {
            $model->load(\Yii::$app->request->post());
            if ($model->validate()) {
                try {
                    $user = new User();
                    $user->username = $model->username;
                    $user->email = $model->email;
                    $user->password = \Yii::$app->getSecurity()->generatePasswordHash($model->password);
                    $user->save();

                    $auth = \Yii::$app->authManager;
                    $authRole = $auth->getRole($model->role);
                    $auth->assign($authRole, $user->getId());

                    foreach ($model->channels as $channel) {
                        $cal = new ChannelAccessLevel();
                        $cal->user_id = $user->id;
                        $cal->channel_type_id = $channel;
                        $cal->save();
                    }
                } catch (\Exception $exception) {
                    \Yii::$app->session->setFlash('warning', 'Ошибка при создании пользователя');
                    return $this->redirect(['user/index']);
                }

                \Yii::$app->session->setFlash('success', 'Пользователь успешно создан');
                return $this->redirect(['user/index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'types' => $types
        ]);
    }

    /**
     * Обновление пользователя
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id) {
        $model = User::findOne(['id' => $id]);
        $types = ChannelType::find()->all();
        $form = new UserForm();
        $form->setAttributes($model->getAttributes(),false);

        $auth = \Yii::$app->authManager;
        $adminIds = $auth->getUserIdsByRole(User::ROLE_ADMIN);

        $form->role = in_array($id, $adminIds) ? User::ROLE_ADMIN : User::ROLE_MANAGER;
        $form->channels = User::getAvailableChannels($id);
        $form->password = '';

        if (\Yii::$app->request->post()) {
            $post = \Yii::$app->request->post('UserForm');

            if (!array_key_exists('email', $post)) {
                $form->setScenario(UserForm::SCENARIO_CHANGE_PASSWORD);
                $form->setAttributes($post);


                if ($form->validate()) {
                    try {
                        $model->password = \Yii::$app->getSecurity()->generatePasswordHash($form->password);
                        $model->save();
                    } catch (\Exception $exception) {
                        \Yii::$app->session->setFlash('warning', 'Ошибка при смене пароля');
                        return $this->redirect(['user/index']);
                    }

                    \Yii::$app->session->setFlash('success', 'Пароль успешно изменен');
                    return $this->redirect(['user/index']);
                }
            } else {
                $form->setScenario(UserForm::SCENARIO_UPDATE);
                $form->setAttributes($post);

                if ($form->validate()) {
                    try {
                        $model->username = $form->username;
                        $model->email = $form->email;
                        $model->save();

                        ChannelAccessLevel::deleteAll(['user_id' => $id]);

                        foreach ($form->channels as $channel) {
                            $cal = new ChannelAccessLevel();
                            $cal->user_id = $id;
                            $cal->channel_type_id = $channel;
                            $cal->save();
                        }

                        $roles = $auth->getRolesByUser($id);

                        foreach ($roles as $role) {
                            $auth->revoke($role, $id);
                        }

                        $authRole = $auth->getRole($form->role);
                        $auth->assign($authRole, $id);
                    } catch (\Exception $exception) {
                        \Yii::$app->session->setFlash('warning', 'Ошибка при обновлении данных пользователя');
                        return $this->redirect(['user/index']);
                    }

                    \Yii::$app->session->setFlash('success', 'Данные пользователя успешно обновлены');
                    return $this->redirect(['user/index']);
                }
            }
        }

        return $this->render('update', [
            'model' => $form,
            'types' => $types,
        ]);
    }

    /**
     * Удаление пользователя
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id) {
        try {
            User::findOne(['id' => $id])->delete();

            $auth = \Yii::$app->authManager;
            $roles = $auth->getRolesByUser($id);

            foreach ($roles as $role) {
                $auth->revoke($role, $id);
            }
        } catch (\Exception $exception) {
            \Yii::$app->session->setFlash('warning', 'Не возможно удалить пользователя. Проверьте взаимосвяси');
            return $this->redirect(['user/index']);
        } catch (\Throwable $e) {
            \Yii::$app->session->setFlash('warning', 'Не возможно удалить пользователя. Проверьте взаимосвяси');
            return $this->redirect(['user/index']);
        }

        \Yii::$app->session->setFlash('success', 'Пользователь успешно удален');
        return $this->redirect(['user/index']);
    }
}