<?php

namespace app\controllers;


use app\models\AllowField;
use app\models\Channel;
use app\models\ChannelAtribute;
use app\models\ChannelType;
use app\models\forms\DynamicModel;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Контроллер для работы с каналами рекламы
 * Class ChannelController
 * @package app\controllers
 */
class ChannelController extends Controller {
    /**
     * Разрешения
     * @return array
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => ['create', 'view', 'delete', 'update'],
                        'allow' => true,
                        'roles' => ['manager', 'admin']
                    ]
                ],
            ],
        ];
    }

    /**
     * Список доступных каналов рекламы
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => User::getAvailableChannels(\Yii::$app->getUser()->id, false),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Создание записи в выбранном канале
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionCreate($id) {
        if (!\Yii::$app->user->can('create', ['id' => $id])) {
            return $this->redirect(['channel/index']);
        }

        $model = new DynamicModel();
        $channelType = ChannelType::findOne(['id' => $id]);
        $allowFields = AllowField::findAll(['channel_types_id' => $id]);

        $model->generate($allowFields);

        if (\Yii::$app->request->post()) {
            $model->load(\Yii::$app->request->post());

            if ($model->validate()) {
                try {
                    $channel = new Channel();
                    $channel->channel_type_id = $id;
                    $channel->user_id = \Yii::$app->user->id;
                    $channel->save();

                    foreach ($allowFields as $allowField) {
                        (new Query())->createCommand()->insert('channel_attributes', [
                            'channel_id' => $channel->id, 'allow_fields_id' => $allowField->id,
                        ])->execute();

                        $lastId = \Yii::$app->db->getLastInsertID();

                        (new Query())->createCommand()->insert("channel_attribute_{$allowField->type}_value", [
                            'channel_attribute_id' => $lastId, 'value' => $model->{$allowField->name},
                        ])->execute();
                    }
                } catch (\Exception $exception) {
                    \Yii::$app->session->setFlash('warning', 'Ошибка при создании пользователя!');
                }

                \Yii::$app->session->setFlash('success', 'Пользователь был создан!');

                return $this->redirect(['channel/view', 'id' => $id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'allowFields' => $allowFields,
            'channelType' => $channelType
        ]);
    }

    /**
     * Отображение записей для выбранного канала
     * Администратору отображаются все пользователи
     * @param $id
     * @return string
     */
    public function actionView($id) {
        $channelType = ChannelType::findOne(['id' => $id]);

        if (!\Yii::$app->user->can('create', ['id' => $id])) {
            return $this->redirect(['channel/index']);
        }

        $columns = [
            ['class' => 'yii\grid\SerialColumn']
        ];

        $sql = Channel::getRowsByChannelType($id, $columns, \Yii::$app->user->id);

        $columns[] = [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Действия',
            'template' => '{delete}{update}',
            'buttons' => [
                'delete' => function($url, $model) {
                    return Html::a(
                    '<span class="glyphicon glyphicon-trash"></span>',
                        Url::to(['channel/delete', 'id' => $model['id']]),
                        [
                            'title' => \Yii::t('yii', 'Удалить'),
                        ]
                    );
                },
                'update' => function($url, $model) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-pencil"></span>',
                        Url::to(['channel/update', 'id' => $model['id']]),
                        [
                            'title' => \Yii::t('yii', 'Редактировать'),
                        ]
                    );
                }
            ]
        ];

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'channelType' => $channelType,
            'id' => $id,
            'columns' => $columns
        ]);
    }

    /**
     * Удаление записи с канала
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id) {
        $channel = Channel::findOne(['id' => $id]);
        $type = $channel->channel_type_id;

        if (!\Yii::$app->user->can('delete', ['channel' => $channel])) {
            return $this->redirect(['channel/view', 'id' => $type]);
        }

        try {
            Channel::deleteAll([
                'id' => $id,
            ]);
        } catch (\Exception $exception) {
            \Yii::$app->session->setFlash('warning', 'Запись канала не может быть удалена. Проверьте взаимосвязи');
            return $this->redirect(['channel/view', 'id' => $type]);
        }

        \Yii::$app->session->setFlash('success', 'Запись канала была успешно удалена');
        return $this->redirect(['channel/view', 'id' => $type]);
    }

    /**
     * Обновление записи канала
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id) {
        $channel = Channel::findOne(['id' => $id]);
        $channelType = ChannelType::findOne(['id' => $channel->channel_type_id]);

        if (!\Yii::$app->user->can('update', ['channel' => $channel])) {
            return $this->redirect(['channel/view', 'id' => $channelType->id]);
        }

        $model = new DynamicModel();
        $allowFields = AllowField::findAll(['channel_types_id' => $channelType->id]);
        $rowChannel = Channel::getRowByChannel($id);

        $model->generate($allowFields);

        if ($rowChannel) {
            foreach ($rowChannel[0] as $key => $value) {
                if (!in_array($key, ['id', 'type', 'channel', 'user_id', 'user_name', 'user_email'])) {
                    $model->{$key} = $value;
                }
            }
        }

        if (\Yii::$app->request->post()) {
            $model->load(\Yii::$app->request->post());

            if ($model->validate()) {
                try {
                    foreach ($allowFields as $allowField) {
                        $attribute = ChannelAtribute::findOne([
                            'channel_id' => $id, 'allow_fields_id' => $allowField->id
                        ]);

                        (new Query())->createCommand()->update("channel_attribute_{$allowField->type}_value", [
                                'value' => $model->{$allowField->name}
                            ], 'channel_attribute_id = :attribute', [
                                'attribute' => $attribute->id
                            ])->execute();
                    }
                } catch (\Exception $exception) {
                    \Yii::$app->session->setFlash('warning', 'Ошибка при обновлении записи канала');
                    return $this->redirect(['channel/view', 'id' => $channelType->id]);
                }

                \Yii::$app->session->setFlash('success', 'Запись канала обновлена');
                return $this->redirect(['channel/view', 'id' => $channelType->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'allowFields' => $allowFields,
            'channelType' => $channelType
        ]);
    }
}