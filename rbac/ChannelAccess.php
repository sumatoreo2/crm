<?php

namespace app\rbac;

use app\models\ChannelAccessLevel;
use yii\rbac\Rule;

class ChannelAccess extends Rule {
    public $name = 'isAccess';

    public function execute($user, $item, $params) {
        return ChannelAccessLevel::find()->where([
            'user_id' => $user,
            'channel_type_id' => $params['id']
        ])->exists();
    }
}