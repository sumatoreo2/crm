<?php

namespace app\rbac;
use yii\rbac\Rule;

class ChannelAuthor extends Rule {
    public $name = 'isAuthor';

    public function execute($user, $item, $params) {
        return \Yii::$app->user->id === $params['channel']->user_id;
    }
}