<?php

return [
    '/' => 'channel/index',
    '/login' => 'site/login',
    '/logout' => 'site/logout',
    '/contact' => 'site/contact',
    '/about' => 'site/about',

    '/user' => 'user/index',
    '/user/create' => 'user/create',
    '/user/update/<id:\d+>' => 'user/update',
    '/user/delete/<id:\d+>' => 'user/delete',

    '/channel' => 'channel/index',
    '/channel/<id:\d+>' => 'channel/view',
    '/channel/delete/<id:\d+>' => 'channel/delete',
    '/channel/update/<id:\d+>' => 'channel/update',
    '/channel/create/<id:\d+>' => 'channel/create',
    
    '/channel/type' => 'type/index',
    '/channel/type/create' => 'type/create',
    '/channel/type/update/<id:\d+>' => 'type/update',
    '/channel/type/delete/<id:\d+>' => 'type/delete',

    '/channel/type/<channel:\d+>/field' => 'fields/index',
    '/channel/type/<channel:\d+>/field/create' => 'fields/create',
    '/channel/type/<channel:\d+>/field/delete/<id:\d+>' => 'fields/delete'
];